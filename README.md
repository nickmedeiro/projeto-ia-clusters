# K-Means clusterização em C++

#### Esta é uma implementação C++ do algoritmo de agrupamento K-Means.

O agrupamento K-means é um tipo de aprendizado não supervisionado, que é usado quando tem-se dados não rotulados (ou seja, dados sem categorias ou grupos definidos). O objetivo deste algoritmo é encontrar grupos nos dados, com o número de grupos representado pela variável K. O algoritmo trabalha iterativamente para atribuir cada ponto de dados a um dos K ​​grupos com base nos recursos fornecidos. Os pontos de dados são agrupados com base na semelhança de recursos.


Esta implementação contém suporte a multithreading, que acelera os cálculos para grandes vetores e milhares de pontos por paralelização.

## Instruções:
A entrada suporta qualquer número de pontos e qualquer número de dimensões. Faça o arquivo "input.txt" de acordo com a necessidade.

* Baixe o arquivo binário "kmeans" do repositório.
* Faça um arquivo "input.txt" (ou outro nome, não faz diferença) com todas as coordenadas dos pontos. O formato deve ser como mostrado abaixo (o exemplo tem coordenadas bidimensionais):

![Input File Syntax](image/input.png)
* Execute o binário kmeans com o nome do arquivo de entrada, número de clusters e diretório de saída como argumentos de linha de comando.

Rode esse commando: 
`
./kmeans input.txt 2 cluster-details
`

Saída:
![Bash Output](image/cmd.png)
* A saída será 2 arquivos no diretório de saída (por exemplo: cluster-details) que contém o ponto central de cada cluster e os detalhes do cluster para cada ponto.

![Clusters File Syntax](image/clusters.png)

-------------------------------
