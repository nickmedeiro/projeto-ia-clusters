#include <omp.h>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

class Point{

    private:
    int pointId;
    int dimensions;
    vector<double> values;

    vector<double> lineToVec(string &line){
        vector<double> values;
        string tmp = "";

        for (int i = 0; i < (int)line.length(); i++){
            if ((48 <= int(line[i]) && int(line[i])  <= 57) || line[i] == '.' 
                || line[i] == '+' || line[i] == '-' || line[i] == 'e') 
                tmp += line[i];    
            else if (tmp.length() > 0){
                values.push_back(stod(tmp));
                tmp = "";
            }
        }
        if (tmp.length() > 0){
            values.push_back(stod(tmp));
            tmp = "";
        }

        return values;
    }

    public:
    Point(int id, string line){ 
        pointId = id;
        values = lineToVec(line);
        dimensions = values.size();
    }

    int getDimensions(){ 
        return dimensions; 
    }

    int getID(){ 
        return pointId; 
    }

    double getVal(int pos){ 
        return values[pos]; 
    }
};

class Cluster{

    private:
    int clusterId;
    int centroidId;
    vector<Point> points;

    public:
    Cluster(int clusterId, Point centroid){
        this->clusterId = clusterId;
        this->centroidId = centroid.getID();
        this->addPoint(centroid);
    }
       
    
    void addPoint(Point p){
        points.push_back(p);
    }

    bool removePoint(int pointId){
        int size = points.size();

        for (int i = 0; i < size; i++){  
            if (points[i].getID() == pointId){      
                points.erase(points.begin() + i);
                return true;
            }
        }

        return false;
    }

    void removeAllPoints(){ 
        points.clear();
    }

    int getId(){ 
        return clusterId; 
    }

    Point getPoint(int pos){ 
        return points[pos]; 
    }

    int getSize(){ 
        return points.size(); 
    }

    int getCentroid(){ 
        return centroidId; 
    }

    void setCentroid(int id){ 
        this->centroidId = id; 
    }
};

class Graph{
    private:
    int K, dimensions, size;
    vector<Cluster> clusters;
    string output_dir;

    public:
    Graph(int K, string output_dir){
        this->K = K;
        this->output_dir = output_dir;
    }

    double euclidean(double x1,double x2,double y1,double y2){
        return sqrt((pow(x1-x2, 2))+(pow(y1-y2, 2)));
    }

    bool validate(int id, vector<int>used){
        for(int i = 0; i < used.size(); i++)
            if(id == used[i])
                return false;        
        
        return true;
    }

    void run(vector<Point> &all_points){
        size = all_points.size();
        dimensions = all_points[0].getDimensions();
        double matrix[size][size];
        double max_value = 0.0;
        pair<int,int> initial_cluster;

        cout << "Gerando grafo completo K" << size << "..." << endl;
        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                double edge = euclidean(all_points[i].getVal(0),all_points[j].getVal(0),all_points[i].getVal(1),all_points[j].getVal(1));  
                matrix[i][j] = edge;
                if(edge > max_value){
                    cout << "Atualizando centroides iniciais... "<< endl;
                    max_value = edge;
                    initial_cluster.first=i+1;
                    initial_cluster.second=j+1;
                }
                //cout << matrix[i][j] << " ";
            }
            //cout << endl << endl;
        }
        cout << "Grafo completo gerado com sucesso !" << endl << endl;

        Cluster cluster1(1, all_points[initial_cluster.first]);
        clusters.push_back(cluster1);
        Cluster cluster2(2, all_points[initial_cluster.second]);
        clusters.push_back(cluster2);

        cout << "Initial Clusters: " << initial_cluster.first << " " << initial_cluster.second << endl;

        vector<int> used_pointIds;
        used_pointIds.push_back(initial_cluster.first);
        used_pointIds.push_back(initial_cluster.second);

        if(K != 2){
            cout << "Definindo novos centroides... "<< endl;
            while(clusters.size() < K){
                double max_dist = 0.0;
                int candidadeId;
                for (int i = 0; i < all_points.size(); i++){
                    if(validate(i+1, used_pointIds)){
                        double total_dist = 0.0;
                        //cout << "Id " << i+1 << " em analise!" << endl;
                        for(int j = 0; j < used_pointIds.size(); j++)
                            total_dist = total_dist + matrix[i][used_pointIds[j]];
    
                        if(total_dist > max_dist){
                            max_dist=total_dist;
                            candidadeId = i+1;
                        }
                    }  
                }
                cout << "Novo centroide adicionado Id " << candidadeId << endl;
                Cluster cluster(clusters.size()+1, all_points[candidadeId-1]);
                clusters.push_back(cluster);
                used_pointIds.push_back(candidadeId);
            }
        }

        cout << endl;
        //cout <<"K = "<< K << " Centroides = " << used_pointIds.size() << endl;
        cout << "Todos os centroides" << endl;
        for(int i=0; i < used_pointIds.size(); i ++)
            cout << used_pointIds[i] << " ";
        cout << endl;


        double candidate[K][size];
        for (int i = 0; i < K; i++){
            for (int j = 0; j < size; j++){
                candidate[i][j]=matrix[used_pointIds[i]-1][j];
                cout << candidate[i][j] << " ";
            }
            cout << endl << endl;
        }

        pair<int,int>index;
        for (int j = 0; j < size; j++){
            if(validate(j+1, used_pointIds)){
                double aux = 1000.0;
                cout << endl << "Analisando Id " << j+1 << endl;
                for (int i = 0; i < K; i++){
                    cout << "linha= " << i << " coluna= " << j << " candidate= " << candidate[i][j] << endl;
                    
                    if(candidate[i][j] < aux){
                        cout << "Entrou!" << endl;
                        aux = candidate[i][j];
                        index.first = i+1;
                        index.second = j+1;
                        cout << "aux= " << aux << endl;
                    }
                }
                cout << endl;
                cout << "Adiconando elemento " << index.second << " ao cluster "<< index.first << endl;
                //cout << "FLAG " << index.second << endl;
                clusters[index.first-1].addPoint(all_points[index.second-1]);
                //cout << "FLAG " << index.second << endl;
                used_pointIds.push_back(index.second);
                //cout << "FLAG " << index.second << endl;
            }
            //cout << j << endl;
        }

        //Prints de todos os clusters
        for(int i=0; i<K ;i++){
            double sum = 0.0;
            cout << "Cluster " << i+1 << ": ";
            for(int j = 0; j < clusters[i].getSize() ; j++){
                Point aux = clusters[i].getPoint(j);
                cout << aux.getID() << " ";
                sum += candidate[i][aux.getID()];
            }
            cout << endl << "Somatorio das Distancias: " << sum << endl;
            cout << endl; 
        }

        
        // Write cluster centers to file
        ofstream outfile;
        outfile.open(output_dir + "/" + to_string(K) + "-clusters.txt");

        if (outfile.is_open()){ 
            for(int i=0; i<K ;i++){
                double sum = 0.0;
                outfile << "Cluster " << i+1 << ": ";
                for(int j = 0; j < clusters[i].getSize() ; j++){
                    Point aux = clusters[i].getPoint(j);
                    outfile << aux.getID() << " ";
                    sum += candidate[i][aux.getID()];
                }
                outfile << endl << "Somatorio das Distancias: " << sum << endl;
                outfile << endl; 
            }
            outfile.close();
        }else
            cout << "Erro: Impossivel escrever em clusters.txt";
        
    }
};

int main(int argc, char **argv){

    // Need 3 arguments (except filename) to run, else exit
    if (argc != 4){
        cout << "Erro: falta argumentos para executar. \n ./kmeans <INPUT> <K> <OUT-DIR>" << endl;
        return 1;
    }

    string output_dir = argv[3];

    // Fetching number of clusters
    int K = atoi(argv[2]);

    // Open file for fetching points
    string filename = argv[1];
    ifstream infile(filename.c_str());

    if (!infile.is_open()){
        cout << "Erro: Falha em abrir a base." << endl;
        return 1;
    }

    // Fetching points from file
    int pointId = 1;
    vector<Point> all_points;
    string line;

    while (getline(infile, line)){
        Point point(pointId, line);
        all_points.push_back(point);
        pointId++;
    }
    
    infile.close();
    cout << "\nDados carregados com sucesso!" << endl
         << endl;

    // Return if number of clusters > number of points
    if ((int)all_points.size() < K){
        cout << "Erro: Numero de clusters informado é maior que numero de pontos." << endl;
        return 1;    
    }

    Graph graph(K, output_dir);
    graph.run(all_points);

    return 0;
}
